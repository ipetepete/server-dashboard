import os, re

from flask import Flask, flash, redirect, render_template, request, session, abort

app = Flask(__name__)

@app.route('/')
def hello():
    p = os.popen("sensors coretemp-isa-0000 ") # lm-sensors 3.5 supports '-j' json ouput
    out = p.read()
    clean_cores = out.split("\n")[3:-2] # strip the unessesary

    cores = map(lambda x: re.split(' +', x)[2], clean_cores)
    cores = list(cores)
    core = 1
    template = "<div><span class='core'>{}: {}</span></div>"
    output = ""
    for i in cores:
        output += template.format(f'Core_{core}', i)
        core += 1
    return output

def get_disk_space():
    # TODO
    pass

if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)

